﻿using Domain.Enums;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models
{
    public class ProjectTask
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Name { get; set; }
        public Project Project { get; set; }
        public ApplicationUser User { get; set; } 
        public Status Statuses { get; set; }
        public int? Progress { get; set; }
        public DateTime? Deadline { get; set; }
        public string Description { get; set; }

    }
}
