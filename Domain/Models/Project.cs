﻿using ProjectManagementSystemMVC.Areas.Identity.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public class Project
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; protected set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; } //unique
        public virtual ICollection<ProjectTask> Tasks { get; set; }
        public ApplicationUser ProjectManager { get; set; }
    }
}