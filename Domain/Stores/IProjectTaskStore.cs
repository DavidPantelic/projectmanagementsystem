﻿using Domain.Enums;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Stores
{
    public interface IProjectTaskStore
    {
        Task<ProjectTask> GetProjectTaskById(string id);
        Task AddTask(ProjectTask projectTask);
        Task Update(ProjectTask projectTask);
        Task Delete(ProjectTask projectTask);
        public bool UserHaveTask(string id);        
    }
}
