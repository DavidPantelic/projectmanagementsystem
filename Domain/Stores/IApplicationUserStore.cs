﻿using ProjectManagementSystemMVC.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Stores
{
    public interface IApplicationUserStore
    {
        Task Add(ApplicationUser applicationUser);
    }
}
