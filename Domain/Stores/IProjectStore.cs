﻿using Domain.Models;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Stores
{
    public interface IProjectStore
    {
        Task<Project> GetByIdAsync(string id);
        IEnumerable<Project> GetAll();
        Task Add(Project project);
        Task Update(Project project);       
        Task AddUser(ApplicationUser user);
        Task Delete(Project project);
        public bool UserManageProject(string id);
    }
}
