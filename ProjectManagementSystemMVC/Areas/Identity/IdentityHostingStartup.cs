﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using ProjectManagementSystemMVC.Data;

[assembly: HostingStartup(typeof(ProjectManagementSystemMVC.Areas.Identity.IdentityHostingStartup))]
namespace ProjectManagementSystemMVC.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("ApplicationDbContextConnection")), ServiceLifetime.Scoped);
                
            });
           
        }
    }
}