﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Models.ApplicationRoleViewModels
{
    public class ApplicationRoleDetailViewModel
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
    }
}
