﻿using ProjectManagementSystemMVC.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Models.ProjectViewModels
{
    public class ProjectEditViewModel
    {
        public string Id { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
        public IEnumerable<UserDetailViewModel> Users { get; set; }
        public string ProjectManagerId { get; set; }
        public string CurrentProjectManger { get; set; }
    }
}
