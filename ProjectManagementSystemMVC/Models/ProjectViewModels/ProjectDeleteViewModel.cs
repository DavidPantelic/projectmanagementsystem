﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Models.ProjectViewModels
{
    public class ProjectDeleteViewModel
    {
        public string Id { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
    }
}
