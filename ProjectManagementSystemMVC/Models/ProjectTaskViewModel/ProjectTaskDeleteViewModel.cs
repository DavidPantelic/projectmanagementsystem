﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Models.ProjectTaskViewModel
{
    public class ProjectTaskDeleteViewModel
    {
        public string Id { get; set; }       
        public string ProjectId { get; set; }
        public string TaskName { get; set; }
        public string Statuses { get; set; }
        public DateTime? Deadline { get; set; }
        public string UserName { get; set; }
    }
}
