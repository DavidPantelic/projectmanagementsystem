﻿using ProjectManagementSystemMVC.Models.ProjectViewModels;
using ProjectManagementSystemMVC.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Models.ProjectTaskViewModel
{
    public class CreateTaskViewModel
    {
        [Required]
        public string ProjectId { get; set; }

        public string UserId { get; set; }

        public IEnumerable<UserDetailViewModel> Users { get; set; }

        public string TaskName { get; set; }

        public string ProjectName { get; set; }

        public DateTime? Deadline { get; set; }
    }
}
