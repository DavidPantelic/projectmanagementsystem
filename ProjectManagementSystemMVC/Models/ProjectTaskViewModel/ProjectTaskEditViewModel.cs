﻿using Domain.Enums;
using ProjectManagementSystemMVC.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Models.ProjectTaskViewModel
{
    public class ProjectTaskEditViewModel
    {
        public string ProjectId { get; set; }

        public string TaskId { get; set; }
        public string UserId { get; set; }

        public IEnumerable<UserDetailViewModel> Users { get; set; }

        public string TaskName { get; set; }

        public string ProjectName { get; set; }

        public DateTime? Deadline { get; set; }

        public string Description { get; set; }

        public Status Statuses { get; set; }

        public int? Progress { get; set; }
    }
}
