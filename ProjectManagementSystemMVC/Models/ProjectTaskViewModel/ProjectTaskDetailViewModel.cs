﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Models.ProjectTaskViewModel
{
    public class ProjectTaskDetailViewModel
    {
        public string Id { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }        
        public string TaskName { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string Status { get; set; }
        public int? Progress { get; set; }
        public DateTime? Deadline { get; set; }
        public string Description { get; set; }

    }
}
