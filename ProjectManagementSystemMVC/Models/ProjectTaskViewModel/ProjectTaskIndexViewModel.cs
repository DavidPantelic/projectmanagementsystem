﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Models.ProjectTaskViewModel
{
    public class ProjectTaskIndexViewModel
    {
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public IEnumerable<ProjectTaskDetailViewModel> Tasks { get; set; }
    }
}
