using Domain.Stores;
using Infrastructure.Stores;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using ProjectManagementSystemMVC.Data;

namespace ProjectManagementSystemMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddRazorPages();

            services.AddScoped<IProjectStore, ProjectStore>();
            services.AddScoped<IProjectTaskStore, ProjectTaskStore>();
            services.AddScoped<IApplicationUserStore, ApplicationUserStore>();

            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = false).AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddAuthorization(options =>
            {
            options.AddPolicy("AdminAccess", policy => policy.RequireRole("Admin"));

            options.AddPolicy("ManagerAccess", policy =>
                policy.RequireAssertion(context =>
                    context.User.IsInRole("Admin") || context.User.IsInRole("Project Manager")));

            options.AddPolicy("DeveloperAccess", policy =>
                policy.RequireAssertion(context =>
                    context.User.IsInRole("Admin") || context.User.IsInRole("Project Manager") || context.User.IsInRole("Developer")));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<ApplicationUser> userManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                // Users
                endpoints.MapControllerRoute(
                    name: "Users",
                    pattern: "users/index",
                    defaults: new { controller = "User", action = "Index" });

                //Projects
                endpoints.MapControllerRoute(
                   name: "ProjectDetail",
                   pattern: "projects/{id?}",
                   defaults: new { controller = "Project", action = "Detail" });

                endpoints.MapControllerRoute(
                    name: "Projects",
                    pattern: "projects/index",
                    defaults: new { controller = "Project", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "Project",
                    pattern: "projects/edit/{id?}",
                    defaults: new { controller = "Project", action = "Edit" });

                //ProjectTask
                endpoints.MapControllerRoute(
                   name: "ProjectTask",
                   pattern: "projects/{id?}/tasks/index",
                   defaults: new { controller = "ProjectTask", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
            });

            ApplicationDbInitializer.SeedUsers(userManager);
        }

        public static class ApplicationDbInitializer
        {
            public static void SeedUsers(UserManager<ApplicationUser> userManager)
            {
                if (userManager.FindByEmailAsync("admin123@gmail.com").Result == null)
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        UserName = "admin123@gmail.com",
                        Email = "admin123@gmail.com",
                        FirstName = "Admin",
                        LastName = "Adminic"
                    };

                    IdentityResult result = userManager.CreateAsync(user, "Admin!123").Result;

                    if (result.Succeeded)
                    {
                        userManager.AddToRoleAsync(user, "Admin").Wait();
                    }
                }
                if (userManager.FindByEmailAsync("manager123@gmail.com").Result == null)
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        UserName = "manager123@gmail.com",
                        Email = "manager123@gmail.com",
                        FirstName = "Manager",
                        LastName = "Manageric"
                    };

                    IdentityResult result = userManager.CreateAsync(user, "Manager!123").Result;

                    if (result.Succeeded)
                    {
                        userManager.AddToRoleAsync(user, "Project Manager").Wait();
                    }
                }
                if (userManager.FindByEmailAsync("developer123@gmail.com").Result == null)
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        UserName = "developer123@gmail.com",
                        Email = "developer123@gmail.com",
                        FirstName = "Developer",
                        LastName = "Developeric"
                    };

                    IdentityResult result = userManager.CreateAsync(user, "Developer!123").Result;

                    if (result.Succeeded)
                    {
                        userManager.AddToRoleAsync(user, "Developer").Wait();
                    }
                }
            }
        }
    }
}