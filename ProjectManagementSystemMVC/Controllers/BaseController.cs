﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProjectManagementSystemMVC.Areas.Identity.Data;

namespace ProjectManagementSystemMVC.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public BaseController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public ApplicationUser GetCurrentUser() 
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            return _userManager.FindByNameAsync(currentUser.Identity.Name).Result;
        }

        public bool IsAdmin()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            var user = _userManager.FindByNameAsync(currentUser.Identity.Name).Result;
            var roles = _userManager.GetRolesAsync(user).Result;
            
            if (roles.Contains("Admin")) 
                return true;
            return false;                                                
        }

        public bool IsManager()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            var user = _userManager.FindByNameAsync(currentUser.Identity.Name).Result;
            var roles = _userManager.GetRolesAsync(user).Result;

            if (roles.Contains("Project Manager"))
                return true;
            return false;
        }

        public bool IsDeveloper()
        {
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            var user = _userManager.FindByNameAsync(currentUser.Identity.Name).Result;
            var roles = _userManager.GetRolesAsync(user).Result;

            if (roles.Contains("Developer"))
                return true;
            return false;
        }

    }  
}