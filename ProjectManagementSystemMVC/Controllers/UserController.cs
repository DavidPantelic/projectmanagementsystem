﻿using Domain.Stores;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using ProjectManagementSystemMVC.Models.ApplicationRoleViewModels;
using ProjectManagementSystemMVC.Models.UserViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Controllers
{
    
    public class UserController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IProjectTaskStore _projectTaskStore;
        private readonly IProjectStore _projectStore;

        public UserController(UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IProjectStore projectStore,
            IProjectTaskStore projectTaskStore) : base(userManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _projectTaskStore = projectTaskStore;
            _projectStore = projectStore;
        }

        
        [Authorize(Policy = "DeveloperAccess")]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var users = _userManager.Users.Select(user => new UserDetailViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email,
                RoleName = _userManager.GetRolesAsync(user).Result.FirstOrDefault()
            });

            return View(await users.ToListAsync());
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UserDetailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = new ApplicationUser
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                UserName = model.Username,
                Email = model.Email
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                throw new ApplicationException($"Unexpected error occurred during creating the user.");
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new ApplicationException($"Unable to load user.");
            }

            var model = new UserEditViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email
            };

            return View(model);
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(UserEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == model.Id);

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.UserName = model.Username;
            user.Email = model.Email;

            await _userManager.UpdateAsync(user);

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == id);
            
            if (user == null)
            {
                return RedirectToAction(nameof(Index));
            }

            if(_projectStore.UserManageProject(id) == true)
            {
                throw new ApplicationException($"Cannot delete user who manage project.");
            }

            if(_projectTaskStore.UserHaveTask(id) == true)
            {
                throw new ApplicationException($"Cannot delete user who has tas.");
            }

            var model = new UserDetailViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email
            };

            return View(model);
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(UserDetailViewModel model)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (user == null)
            {
                return RedirectToAction(nameof(Index));
            }
            await _userManager.DeleteAsync(user);

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpGet]
        public async Task<IActionResult> AddRole(string id)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user.");
            }

            var roles = _roleManager.Roles
               .OrderBy(x => x.Name)
               .Select(r => new ApplicationRoleDetailViewModel
               {
                   Id = r.Id,
                   RoleName = r.Name
               });

            var model = new UserDetailViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email,
                Roles = roles,
                RoleName = _userManager.GetRolesAsync(user).Result.FirstOrDefault()
            };

            return View(model);
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddRole(UserRoleViewModel model)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == model.UserId);

            var roles = _userManager.GetRolesAsync(user).Result;

            if (roles.Any())
            {
                var currentRole = _userManager.GetRolesAsync(user).Result.FirstOrDefault();
                _userManager.RemoveFromRoleAsync(user, currentRole).Wait();
            }
                      
            _userManager.AddToRoleAsync(user, model.RoleName).Wait();
            
            return RedirectToAction(nameof(Index));
        }
    }
}