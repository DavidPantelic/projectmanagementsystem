﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Enums;
using Domain.Models;
using Domain.Stores;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using ProjectManagementSystemMVC.Models.ApplicationRoleViewModels;
using ProjectManagementSystemMVC.Models.ProjectTaskViewModel;
using ProjectManagementSystemMVC.Models.ProjectViewModels;
using ProjectManagementSystemMVC.Models.UserViewModels;

namespace ProjectManagementSystemMVC.Controllers
{
    public class ProjectTaskController : BaseController
    {
        private readonly IProjectStore _projectStore;
        private readonly IProjectTaskStore _projectTaskStore;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public ProjectTaskController(IProjectTaskStore projectTaskStore, IProjectStore projectStore, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager) : base(userManager)
        {
            _projectTaskStore = projectTaskStore;
            _projectStore = projectStore;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string id)
        {
            var project = await _projectStore.GetByIdAsync(id);
            if (project == null)
            {
                return NotFound();
            }
            var tasks = project.Tasks.Select(x => new ProjectTaskDetailViewModel
            {
                Id = x.Id,
                TaskName = x.Name,
                UserName = x.User.UserName,
                Status = x.Statuses.ToString(),
                Progress = x.Progress,
                Deadline = x.Deadline,
                Description = x.Description,
                UserId = x.User.Id
            }).ToList();

            var model = new ProjectTaskIndexViewModel
            {
                ProjectId = id,
                ProjectName = project.ProjectName,
                Tasks = tasks
            };

            ViewBag.CurrentUserId = GetCurrentUser().Id;
            ViewBag.IsAdmin = IsAdmin();

            return View(model);
        }
        [Authorize(Policy = "ManagerAccess")]
        [HttpGet]
        public async Task<IActionResult> AddTask(string id)
        {

            var users = _userManager.Users.Select(user => new UserDetailViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email
            }).ToList();

            var project = await _projectStore.GetByIdAsync(id);

            var model = new CreateTaskViewModel
            {
                ProjectId = id,
                Users = users,
                ProjectName = project.ProjectName
            };

            return View(model);
        }

        [Authorize(Policy = "ManagerAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTask(CreateTaskViewModel model)
        {
            var project = await _projectStore.GetByIdAsync(model.ProjectId);

            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == model.UserId);

            var task = new ProjectTask
            {
                Name = model.TaskName,
                Project = project,
                User = user,
                Statuses = Status.New,
                Deadline = model.Deadline
            };

            await _projectTaskStore.AddTask(task);

            return RedirectToAction(nameof(Index), new { project.Id });
        }

        [Authorize(Policy = "DeveloperAccess")]
        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {            

            var projectTask = await _projectTaskStore.GetProjectTaskById(id);

            var currentUser = projectTask.User.Id;

            if (projectTask == null)
            {
                throw new ApplicationException($"Unable to find Task.");
            }

            var users = _userManager.Users.Select(user => new UserDetailViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email
            }).ToList();

            var model = new ProjectTaskEditViewModel
            {                
                TaskId = id,
                Users = users,
                UserId = currentUser,
                TaskName = projectTask.Name,
                Deadline = projectTask.Deadline,
                Description = projectTask.Description,
                ProjectId = projectTask.Project.Id,
                Progress = projectTask.Progress
            };

            ViewBag.IsManager = IsManager();
            ViewBag.IsDeveloper = IsDeveloper();
            ViewBag.IsAdmin = IsAdmin();

            return View(model);
        }

        [Authorize(Policy = "DeveloperAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProjectTaskEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            
            var projectTask = await _projectTaskStore.GetProjectTaskById(model.TaskId);
           
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == model.UserId);

            projectTask.Name = model.TaskName;
            projectTask.User = user;
            projectTask.Statuses = model.Statuses;
            projectTask.Description = model.Description;
            projectTask.Deadline = model.Deadline;
            projectTask.Progress = model.Progress;

            await _projectTaskStore.Update(projectTask);

            return RedirectToAction(nameof(Index), new { projectTask.Project.Id });
        }
        [Authorize(Policy = "AdminAccess")]
        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            var projectTask = await _projectTaskStore.GetProjectTaskById(id);
            if (projectTask == null)
            {
                return RedirectToAction(nameof(Index));
            }

            var model = new ProjectTaskDeleteViewModel
            {
                Id = id,
                ProjectId = projectTask.Project.Id,
                TaskName = projectTask.Name,
                Statuses = projectTask.Statuses.ToString(),
                Deadline = projectTask.Deadline,
                UserName = projectTask.User.UserName
            };

            return View(model);
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ProjectTaskDeleteViewModel model)
        {
            
            var projectTask = await _projectTaskStore.GetProjectTaskById(model.Id);

            var id = projectTask.Project.Id;

            if (projectTask == null)
            {
                return RedirectToAction(nameof(Index));
            }
            await _projectTaskStore.Delete(projectTask);

            return RedirectToAction(nameof(Index), new { id });
        }
    }
}
