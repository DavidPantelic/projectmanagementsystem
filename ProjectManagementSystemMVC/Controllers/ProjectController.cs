﻿using Domain.Models;
using Domain.Stores;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using ProjectManagementSystemMVC.Models.ProjectTaskViewModel;
using ProjectManagementSystemMVC.Models.ProjectViewModels;
using ProjectManagementSystemMVC.Models.UserViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagementSystemMVC.Controllers
{
    public class ProjectController : BaseController
    {
        private readonly IProjectStore _projectStore;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public ProjectController(IProjectStore projectStore, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager) : base(userManager)
        {
            _projectStore = projectStore;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public IActionResult Index()
        {
            var projects = _projectStore.GetAll().Select(x => new ProjectDetailViewModel
            {
                Id = x.Id,
                ProjectName = x.ProjectName,
                ProjectCode = x.ProjectCode
            });

            var model = new ProjectIndexViewModel
            {
                Projects = projects
            };

            ViewBag.IsAdmin = IsAdmin();

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Detail(string id)
        {
            var project = await _projectStore.GetByIdAsync(id);
                                                  
            var tasks = project.Tasks.Select(c => new ProjectTaskDetailViewModel
            {
                TaskName = c.Name,               
                Status = c.Statuses.ToString(),
                Progress = c.Progress,
                Description = c.Description,
                Deadline = c.Deadline,
                UserName = c.User.UserName
            }).ToList();

            var model = new ProjectDetailViewModel
            {
                Id = project.Id,
                ProjectName = project.ProjectName,               
                Tasks = tasks,
                ProjectCode = project.ProjectCode
            };
                    
            return View(model);
        }

        [Authorize(Policy = "ManagerAccess")]
        [HttpGet]
        public IActionResult Create()
        {
            var users = _userManager.GetUsersInRoleAsync("Project Manager").Result.Select(user => new UserDetailViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email
            }).ToList();

            var model = new ProjectDetailViewModel(10)
            {
                Users = users                
            };

            return View(model);
        }

        [Authorize(Policy = "ManagerAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProjectDetailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var projectManager = _userManager.Users.SingleOrDefault(x => x.Id == model.ProjectManagerId);

            await _projectStore.Add(new Project
            {
                ProjectName = model.ProjectName,
                ProjectManager = projectManager,
                ProjectCode = model.ProjectCode
            });;

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            var project = await _projectStore.GetByIdAsync(id);

            if (project == null)
            {
                throw new ApplicationException($"Unable to load project.");
            }

            var users = _userManager.GetUsersInRoleAsync("Project Manager").Result.Select(user => new UserDetailViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.UserName,
                Email = user.Email
            }).ToList();

            var projectManager = project.ProjectManager.UserName;

            var model = new ProjectEditViewModel
            {
                Id = project.Id,
                ProjectName = project.ProjectName,
                Users = users,
                CurrentProjectManger = projectManager,
                ProjectCode = project.ProjectCode
            };

            return View(model);
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProjectEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var project = await _projectStore.GetByIdAsync(model.Id);
            var projectManager = _userManager.Users.SingleOrDefault(x => x.Id == model.ProjectManagerId);

            project.ProjectName = model.ProjectName;
            project.ProjectManager = projectManager;

            await _projectStore.Update(project);

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Policy = "AdminAccess")]
        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            var project = await _projectStore.GetByIdAsync(id);
            if (project == null)
            {
                return RedirectToAction(nameof(Index));
            }

            var model = new ProjectDeleteViewModel
            {
               Id = id,
               ProjectName = project.ProjectName,
                ProjectCode = project.ProjectCode
            };

            return View(model);
        }
        [Authorize(Policy = "AdminAccess")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ProjectDeleteViewModel model)
        {
            var project = await _projectStore.GetByIdAsync(model.Id);
            if (project == null)
            {
                return RedirectToAction(nameof(Index));
            }
            await _projectStore.Delete(project);

            return RedirectToAction(nameof(Index));
        }
    }
}