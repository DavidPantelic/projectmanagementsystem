﻿using Domain.Models;
using Domain.Stores;
using Microsoft.EntityFrameworkCore;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using ProjectManagementSystemMVC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Stores
{
    public class ProjectStore : IProjectStore
    {
        private readonly ApplicationDbContext _context;

        public ProjectStore(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Project> GetByIdAsync(string id)
        {
            return await _context.Projects
                .Include(x => x.Tasks)
                .ThenInclude(c => c.User)
                .Include(x => x.ProjectManager)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            foreach (var p in _context.Projects)
                yield return p;
        }

        public async Task Add(Project project)
        {
            await _context.AddAsync(project);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Project project)
        {
            _context.Update(project);
            await _context.SaveChangesAsync();
        }

        public async Task AddUser(ApplicationUser user)
        {
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Project project)
        {
            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
        }
        public bool UserManageProject(string id)
        {
            var user = _context.Projects.Select(x => x.ProjectManager).Where(x => x.Id == id).SingleOrDefault();
            if (user != null) return true;
            return false;
        }      
    }
}
