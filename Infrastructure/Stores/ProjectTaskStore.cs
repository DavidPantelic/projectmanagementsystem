﻿using Domain.Enums;
using Domain.Models;
using Domain.Stores;
using Microsoft.EntityFrameworkCore;
using ProjectManagementSystemMVC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Stores
{
    public class ProjectTaskStore : IProjectTaskStore
    {
        private readonly ApplicationDbContext _context;

        public ProjectTaskStore(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task AddTask(ProjectTask projectTask)
        {
            await _context.AddAsync(projectTask);
            await _context.SaveChangesAsync();
        }

        public async Task<ProjectTask> GetProjectTaskById(string id)
        {
            return await _context.Tasks
                .Include(x => x.Project)
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task Update(ProjectTask projectTask)
        {
            _context.Update(projectTask);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(ProjectTask projectTask)
        {
            _context.Tasks.Remove(projectTask);
            await _context.SaveChangesAsync();
        }
        public bool UserHaveTask(string id)
        {
            var user = _context.Tasks.Select(x => x.User).Where(x => x.Id == id).SingleOrDefault();
            if (user != null) return true;
            return false;
        }        
    }
}
