﻿using Domain.Stores;
using Microsoft.AspNetCore.Identity;
using ProjectManagementSystemMVC.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Stores
{
    public class ApplicationUserStore : IApplicationUserStore
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public ApplicationUserStore(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task Add(ApplicationUser applicationUser)
        {
            await _userManager.CreateAsync(applicationUser);
        }
        
    }
}
