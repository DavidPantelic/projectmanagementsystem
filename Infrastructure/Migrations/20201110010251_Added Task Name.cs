﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class AddedTaskName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "459d5ee7-9bf1-4e1b-a9b3-67d87d7b61c4");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ac0c9fdd-9b8c-4f82-acb1-cb96e62562fd");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b5904f94-4bcf-4fab-af85-d08c8e6cbb6a");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Tasks",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "15be70fc-d473-423e-9a31-5933af586a87", "7da79f9b-2d09-49d4-a0c5-23d0fd5026fd", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "215c1a4d-5d76-49ef-8acb-87c009c68690", "6f1affa8-a316-4a4e-bdaf-2180ad0280e6", "Project Manager", "PROJECT MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "a3fdad25-d086-4105-b6de-17a4e9c51616", "d2a718e2-40e3-494d-aff7-95a4877e586d", "Developer", "DEVELOPER" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "15be70fc-d473-423e-9a31-5933af586a87");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "215c1a4d-5d76-49ef-8acb-87c009c68690");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a3fdad25-d086-4105-b6de-17a4e9c51616");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Tasks");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "b5904f94-4bcf-4fab-af85-d08c8e6cbb6a", "790a7197-4895-40c0-a141-24ccee2b9ae1", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "459d5ee7-9bf1-4e1b-a9b3-67d87d7b61c4", "7269e37f-9d8f-4ddf-aff7-9c97dba9f903", "Project Manager", "PROJECT MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "ac0c9fdd-9b8c-4f82-acb1-cb96e62562fd", "a3d2656b-b454-4b7b-9d49-63466d3f372c", "Developer", "DEVELOPER" });
        }
    }
}
