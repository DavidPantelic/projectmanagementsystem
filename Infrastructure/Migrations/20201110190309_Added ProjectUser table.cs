﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class AddedProjectUsertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectApplicationUser_AspNetUsers_ApplicationUserId",
                table: "ProjectApplicationUser");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectApplicationUser_Projects_ProjectId",
                table: "ProjectApplicationUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProjectApplicationUser",
                table: "ProjectApplicationUser");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "15be70fc-d473-423e-9a31-5933af586a87");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "215c1a4d-5d76-49ef-8acb-87c009c68690");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a3fdad25-d086-4105-b6de-17a4e9c51616");

            migrationBuilder.RenameTable(
                name: "ProjectApplicationUser",
                newName: "ProjectApplicationUsers");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectApplicationUser_ApplicationUserId",
                table: "ProjectApplicationUsers",
                newName: "IX_ProjectApplicationUsers_ApplicationUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProjectApplicationUsers",
                table: "ProjectApplicationUsers",
                columns: new[] { "ProjectId", "ApplicationUserId" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "12eefe4d-9041-4ce0-8819-3ae5344a690f", "aa330256-58d2-47d8-a84a-3e7898895153", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "38cf8eda-c555-40cf-b585-26fa4ea62b6d", "0b67eff9-b68c-4526-b965-3fa22d3e08e0", "Project Manager", "PROJECT MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "e0183ff3-ab20-40e0-a12a-d2d5ab921b7a", "cf4bb403-0bf7-4009-9f7e-5d7c9aa7b2d6", "Developer", "DEVELOPER" });

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectApplicationUsers_AspNetUsers_ApplicationUserId",
                table: "ProjectApplicationUsers",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectApplicationUsers_Projects_ProjectId",
                table: "ProjectApplicationUsers",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectApplicationUsers_AspNetUsers_ApplicationUserId",
                table: "ProjectApplicationUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectApplicationUsers_Projects_ProjectId",
                table: "ProjectApplicationUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProjectApplicationUsers",
                table: "ProjectApplicationUsers");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "12eefe4d-9041-4ce0-8819-3ae5344a690f");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "38cf8eda-c555-40cf-b585-26fa4ea62b6d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e0183ff3-ab20-40e0-a12a-d2d5ab921b7a");

            migrationBuilder.RenameTable(
                name: "ProjectApplicationUsers",
                newName: "ProjectApplicationUser");

            migrationBuilder.RenameIndex(
                name: "IX_ProjectApplicationUsers_ApplicationUserId",
                table: "ProjectApplicationUser",
                newName: "IX_ProjectApplicationUser_ApplicationUserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProjectApplicationUser",
                table: "ProjectApplicationUser",
                columns: new[] { "ProjectId", "ApplicationUserId" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "15be70fc-d473-423e-9a31-5933af586a87", "7da79f9b-2d09-49d4-a0c5-23d0fd5026fd", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "215c1a4d-5d76-49ef-8acb-87c009c68690", "6f1affa8-a316-4a4e-bdaf-2180ad0280e6", "Project Manager", "PROJECT MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "a3fdad25-d086-4105-b6de-17a4e9c51616", "d2a718e2-40e3-494d-aff7-95a4877e586d", "Developer", "DEVELOPER" });

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectApplicationUser_AspNetUsers_ApplicationUserId",
                table: "ProjectApplicationUser",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectApplicationUser_Projects_ProjectId",
                table: "ProjectApplicationUser",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
