﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class addenumstatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "008618a9-7147-4446-b381-4a939e77877d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "506b5678-bb5d-43fd-91ab-288e14ee104d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a3658986-e94f-42e2-b34c-7030f73547af");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Tasks");

            migrationBuilder.AddColumn<string>(
                name: "Statuses",
                table: "Tasks",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "ee463762-93e2-406b-aeea-3b8745987a9f", "6f25f97e-fc9e-466d-905a-52f903205024", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "e1edaec1-f763-40fa-8414-a91788a1d338", "43a0181a-316d-45a7-8c15-ed350c6221bd", "Project Manager", "PROJECT MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "48e919e2-f4a7-4b92-962a-1c2a3e0e7b6d", "35d07c10-6f1a-44ad-83d7-3d1b47bc77bc", "Developer", "DEVELOPER" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "48e919e2-f4a7-4b92-962a-1c2a3e0e7b6d");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e1edaec1-f763-40fa-8414-a91788a1d338");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "ee463762-93e2-406b-aeea-3b8745987a9f");

            migrationBuilder.DropColumn(
                name: "Statuses",
                table: "Tasks");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "506b5678-bb5d-43fd-91ab-288e14ee104d", "9a764afc-235e-4604-8a6f-b176220032de", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "008618a9-7147-4446-b381-4a939e77877d", "1af46ade-7f6a-448d-a105-0c4d3cc0450f", "Project Manager", "PROJECT MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "a3658986-e94f-42e2-b34c-7030f73547af", "8cd71432-ab68-4e9b-afd4-802158bfcdec", "Developer", "DEVELOPER" });
        }
    }
}
