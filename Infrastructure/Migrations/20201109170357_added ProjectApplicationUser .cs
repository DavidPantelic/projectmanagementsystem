﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class addedProjectApplicationUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3dd01ef1-ad4c-4cdf-92dd-18cfc935be3e");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3f623be8-5360-461a-87ac-296b62104df4");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d697956b-5ea1-4a1b-8a00-24b09d94dc0b");

            migrationBuilder.CreateTable(
                name: "ProjectApplicationUser",
                columns: table => new
                {
                    ProjectId = table.Column<string>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectApplicationUser", x => new { x.ProjectId, x.ApplicationUserId });
                    table.ForeignKey(
                        name: "FK_ProjectApplicationUser_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectApplicationUser_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "1869e711-d6df-4cf9-aa98-dbb154005042", "b265e61b-84fb-4484-86d8-fde95d46fbfb", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "330921f6-6e2a-4378-82a2-5b0ce58ff13b", "932ac744-7cc2-4ef6-aff7-3a3fa2a2859a", "Project Manager", "PROJECT MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "7ee39b87-2e17-4d56-9e65-6ce430f579e5", "0eba6d9f-9ecb-4664-80b1-562a0aa8261b", "Developer", "DEVELOPER" });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectApplicationUser_ApplicationUserId",
                table: "ProjectApplicationUser",
                column: "ApplicationUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectApplicationUser");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1869e711-d6df-4cf9-aa98-dbb154005042");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "330921f6-6e2a-4378-82a2-5b0ce58ff13b");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7ee39b87-2e17-4d56-9e65-6ce430f579e5");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "3dd01ef1-ad4c-4cdf-92dd-18cfc935be3e", "12df7828-46fd-4b69-ad7d-a373dd5fba79", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "d697956b-5ea1-4a1b-8a00-24b09d94dc0b", "9c1ece39-b70e-4740-b5ca-cb79e44c60b5", "Project Manager", "PROJECT MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "3f623be8-5360-461a-87ac-296b62104df4", "5b8a69ec-884d-429e-a844-3a1ab1a6f0bf", "Developer", "DEVELOPER" });
        }
    }
}
