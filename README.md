# Project Management System
***
A project management application for managing small projects.

## Prerequisites
- Visual Studio 2019
- SQL Server 2019

## Getting Started
***
After opening solution set up your connection string in appsettings.json file and run the snippet below to have an existing database hosted in MSSQLLocalDB.
- PM (Package Manager Console) > update-database

## Technologies

 ASP.NET MVC, MSSQL Server, Entity Framework Core, .NET Core Identity, HTML, CSS

 